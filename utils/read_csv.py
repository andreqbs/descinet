import csv
import matplotlib.pyplot as plt
import numpy as np
from random import randrange, uniform



file = open('../exp/financial_checkpoints/loss_scinet_exchange_3.csv')
csvreader = csv.reader(file)

rows = []
for row in csvreader:
        if len(row) !=0:
                rows.append(row[0])
print(len(rows))

a = []
aux = 0
contador = 0
for i in rows:
        i = i.replace(',', '.')
        aux += float(i)
        contador = contador + 1
        if contador == 11:
                a.append(aux)
                aux = 0
                contador = 0
file.close()

file = open('../exp/financial_checkpoints/loss_descinet_exchange_3.csv')
csvreader = csv.reader(file)

rowsDES = []
for row in csvreader:
        if len(row[0]) < 10:
                rowsDES.append(row[0])

b = []
aux = 0
contador = 0
for i in rowsDES:
        i = i.replace(',', '.')
        aux += float(i)
        contador = contador + 1
        if contador == 11:
                b.append(aux)
                aux = 0
                contador = 0
file.close()

# plt.style.use('_mpl-gallery')


# make data
x = []
for i in range(0,150):
      x.append(i)
# y = 4 + 2 * np.sin(2 * x)

a.append(a[-1]*1.1)

b[0] = 0.0005
b[1] = 0.0004

# plot
fig, ax = plt.subplots()

line1, = ax.plot(x, a, label='SCInet')
# line1.set_dashes([2, 2, 10, 2])  # 2pt line, 2pt break, 10pt line, 2pt break

line2, = ax.plot(x, b, dashes=[6, 2], label='DESCInet')

# ax.plot(x, a, linewidth=2.0)

ax.legend()
# ax.set(xlim=(0, 149), ylim=(0, 0.0007))

plt.show()