import math
import torch.nn.functional as F
from torch.autograd import Variable
from torch import nn
import torch
import argparse
import numpy as np

class Splitting(nn.Module):
    def __init__(self, c_level, side_tree, data_atual=None, dataset=None):
        super(Splitting, self).__init__()
        self.nivel_atual = c_level
        self.dados = data_atual
        self.side_tree = side_tree
        self.dataset = dataset

    def shapeData(self, dataset):
        shapeList = None
        # ETTh1 or ETTh2 univariate
        if dataset[2] == 'S':
            if (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 64:
                shapeList = {32: 1, 16: 2, 8: 4, 4: 8}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 96:
                shapeList = {48: 1, 24: 2, 12: 4, 6: 8}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 720:
                shapeList = {360: 1, 180: 2, 90: 4, 45: 8}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 736:
                shapeList = {368: 1, 184: 2, 92: 4, 46: 8, 23: 16}  # TODO EeTH2
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 336:
                shapeList = {168: 1, 84: 2, 42: 4, 21: 8}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 288:
                shapeList = {144: 1, 72: 2, 36: 4, 18: 4}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 384:
                shapeList = {192: 1, 96: 2, 48: 4, 24: 8}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 672:
                shapeList = {336: 1, 168: 2, 84: 4, 42: 8, 21: 16}
        else:
            if (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 48:
                shapeList = {24: 7, 12: 14, 6: 28, 3: 56, 36: 7, 18: 14, 9: 28} # TODO ETTH1 e ETTH2
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 96:
                shapeList = {48: 7, 24: 14, 12: 28, 6: 56, 36: 14, 18: 28, 9: 56}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 288:
                shapeList = {288: 7, 144: 14, 72: 28, 36: 56}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 336:
                shapeList = {168: 7, 84: 14, 42: 28, 21: 56}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 384:
                shapeList = {192: 7, 96: 14, 48: 28, 24: 56}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2' or dataset[0] == 'ETTm1') and dataset[1] == 672:
                shapeList = {336: 7, 168: 14, 84: 28, 42: 56, 21: 112}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2') and dataset[1] == 720:
                shapeList = {360: 7, 180: 14, 90: 28, 45: 56}
            elif (dataset[0] == 'ETTh1' or dataset[0] == 'ETTh2') and dataset[1] == 736:
                shapeList = {368: 7, 184: 14, 92: 28, 46: 56, 23: 112}
        if dataset[0] == 'solar_AL':
                shapeList = {80: 137, 40: 274, 20: 548, 10: 1096}
        elif dataset[0] == 'electricity' and dataset[1] == 168:
            shapeList = {84: 321, 42: 642, 21: 1284}
        elif dataset[0] == 'electricity' and dataset[1] == 96:
            shapeList = {48: 321, 24: 642, 12: 1284}
        elif dataset[0] == 'exchange_rate' and dataset[1] == 168:
            shapeList = {84: 8, 42: 16, 21: 32, 10: 64}
        elif dataset[0] == 'exchange_rate' and dataset[1] == 96:
            shapeList = {48: 8, 24: 16, 12: 32, 6: 64}

        return shapeList

    def chooseData(self, shape, list):
        shapeTotal = 0
        listFinal = []
        for item in list:
            if item.shape[2] == shape:
                return [item]
        for item in list:
            shapeTotal += item.shape[2]
            listFinal.append(item)
            if shapeTotal == shape:
                return listFinal


    def downsampling(self, actual_input, list_input, even_odd):
        shapeList = self.shapeData(self.dataset)
        new_shape = int(actual_input.shape[1] / 2)
        stack_channels = []
        data = self.chooseData(shapeList[new_shape], list_input)
        if len(data) == 1:
            y = data[0]
            return y
        for item in data:
            shape_item = int(item.shape[1] / new_shape)
            if even_odd == 'e':
                seq_aux = item[:, ::shape_item, :]
            else:
                seq_aux = item[:, 1::shape_item, :]
            seq_aux = seq_aux.permute(0, 2, 1)
            stack_channels.append(seq_aux)
        y = stack_channels[0]
        for idx in range(len(stack_channels) - 1):
            if not y.shape[2] != stack_channels[idx + 1].shape[2]:
                y = torch.hstack((y, stack_channels[idx + 1]))
        y = y.permute(0, 2, 1)
        return y

    def even(self, x):

        if type(x) == list:
            x1 = x.copy()
            x = x[0]
            x2 = x1[2]
            x3 = x2['even']
            z = self.downsampling(x, x3, 'e')
            return z

        y = x[:, ::2, :]

        return y

    def odd(self, x):
        if type(x) == list:
            x1 = x.copy()
            x = x[0]
            x2 = x1[2]
            x3 = x2['odd']
            z = self.downsampling(x, x3, 'o')

            return z

        y = x[:, 1::2, :]
        return y

    def forward(self, x):
        '''Returns the odd and even part'''
        return (self.even(x), self.odd(x))


class Interactor(nn.Module):
    def __init__(self, side_tree, num_levels, c_level, in_planes, splitting=True,
                 kernel=5, dropout=0.5, groups=1, hidden_size=1, INN=True,dataset=None):
        super(Interactor, self).__init__()
        self.modified = INN
        self.kernel_size = kernel
        self.dilation = 1
        self.dropout = dropout
        self.hidden_size = hidden_size
        self.groups = groups
        if self.kernel_size % 2 == 0:
            pad_l = self.dilation * (self.kernel_size - 2) // 2 + 1  # by default: stride==1
            pad_r = self.dilation * (self.kernel_size) // 2 + 1  # by default: stride==1

        else:
            pad_l = self.dilation * (self.kernel_size - 1) // 2 + 1  # we fix the kernel size of the second layer as 3.
            pad_r = self.dilation * (self.kernel_size - 1) // 2 + 1
        self.splitting = splitting
        self.split = Splitting(c_level, side_tree, None, dataset)
        self.c_level = c_level
        self.num_levels = num_levels
        self.side_tree = side_tree

        modules_P = []
        modules_U = []
        modules_psi = []
        modules_phi = []
        prev_size = num_levels - c_level
        # ETTH
        if prev_size == 3:
            prev_size = 4
        elif prev_size == 4:
            prev_size = 8
        elif prev_size == 5:
            prev_size = 16
        # # Exchange e Electricity
        # if prev_size == 3:
        #     prev_size = 4

        size_hidden = self.hidden_size
        modules_P += [
            nn.ReplicationPad1d((pad_l, pad_r)),
            nn.Conv1d(in_planes * prev_size, int(in_planes * size_hidden) * prev_size,
                      kernel_size=self.kernel_size, dilation=self.dilation, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Dropout(self.dropout),
            nn.Conv1d(int(in_planes * size_hidden) * prev_size, in_planes * prev_size,
                      kernel_size=3, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Conv1d(in_planes * prev_size, in_planes * prev_size,
                      kernel_size=1, stride=1, groups=self.groups),
            nn.Tanh()
        ]
        modules_U += [
            nn.ReplicationPad1d((pad_l, pad_r)),
            nn.Conv1d(in_planes * prev_size, int(in_planes * size_hidden) * prev_size,
                      kernel_size=self.kernel_size, dilation=self.dilation, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Dropout(self.dropout),
            nn.Conv1d(int(in_planes * size_hidden) * prev_size, in_planes * prev_size,
                      kernel_size=3, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Conv1d(in_planes * prev_size, in_planes * prev_size,
                      kernel_size=1, stride=1, groups=self.groups),
            nn.Tanh()
        ]

        modules_phi += [
            nn.BatchNorm1d(in_planes * prev_size),
            nn.ReplicationPad1d((pad_l, pad_r)),
            nn.Conv1d(in_planes * prev_size, int(in_planes * size_hidden) * prev_size,
                      kernel_size=self.kernel_size, dilation=self.dilation, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Dropout(self.dropout),
            nn.Conv1d(int(in_planes * size_hidden) * prev_size, in_planes * prev_size,
                      kernel_size=3, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Conv1d(in_planes * prev_size, in_planes * prev_size,
                      kernel_size=1, stride=1, groups=self.groups),
            nn.Tanh()
        ]
        modules_psi += [
            # nn.BatchNorm1d(in_planes * prev_size),
            nn.ReplicationPad1d((pad_l, pad_r)),
            nn.Conv1d(in_planes * prev_size, int(in_planes * size_hidden) * prev_size,
                      kernel_size=self.kernel_size, dilation=self.dilation, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Dropout(self.dropout),
            nn.Conv1d(int(in_planes * size_hidden) * prev_size, in_planes * prev_size,
                      kernel_size=3, stride=1, groups=self.groups),
            nn.LeakyReLU(negative_slope=0.01, inplace=True),
            nn.Conv1d(in_planes * prev_size, in_planes * prev_size,
                      kernel_size=1, stride=1, groups=self.groups),
            nn.Tanh()
        ]
        self.phi = nn.Sequential(*modules_phi)
        self.psi = nn.Sequential(*modules_psi)
        self.P = nn.Sequential(*modules_P)
        self.U = nn.Sequential(*modules_U)

    def forward(self, x):
        if self.splitting:
            (x_even, x_odd) = self.split(x)
        else:
            (x_even, x_odd) = x

        if self.modified:
            x_even = x_even.permute(0, 2, 1)
            x_odd = x_odd.permute(0, 2, 1)

            if not type(x) == list:
                lista_saida_nova = {
                    'even': [x_even.permute(0, 2, 1)],
                    'odd': [x_odd.permute(0, 2, 1)],
                    'estado_atual': self.c_level,
                    'estado_anterior': self.c_level
                }

            # BATCH, CANAIS, SEQUENCIA
            aux1 = self.phi(x_even)
            aux2 = self.psi(x_odd)

            d = x_odd.mul(torch.exp(aux1))
            c = x_even.mul(torch.exp(aux2))

            x_even_update = c + self.U(d)
            x_odd_update = d - self.P(c)

            if type(x) == list:
                lista_saida_nova = x[2]
                lista_saida_nova['even'].append(x_even_update.permute(0, 2, 1))
                lista_saida_nova['odd'].append(x_odd_update.permute(0, 2, 1))
                lista_saida_nova['estado_anterior'] = lista_saida_nova['estado_atual']
                lista_saida_nova['estado_atual'] = self.c_level


            else:
                lista_saida_nova['even'].append(x_even_update.permute(0, 2, 1))
                lista_saida_nova['odd'].append(x_odd_update.permute(0, 2, 1))

            lista_saida_nova['level_atual'] = self.c_level


            return (x_even_update, x_odd_update, lista_saida_nova)

            # return (x_even_update, x_odd_update)

        else:
            x_even = x_even.permute(0, 2, 1)
            x_odd = x_odd.permute(0, 2, 1)

            d = x_odd - self.P(x_even)
            c = x_even + self.U(d)

            return (c, d)


class InteractorLevel(nn.Module):
    def __init__(self, side_tree, num_levels, c_level, in_planes, kernel, dropout, groups, hidden_size, INN,dataset=None):
        super(InteractorLevel, self).__init__()
        self.level = Interactor(side_tree=side_tree, num_levels=num_levels, c_level=c_level,
                                in_planes=in_planes, splitting=True,
                                kernel=kernel, dropout=dropout, groups=groups, hidden_size=hidden_size, INN=INN, dataset=dataset)

    def forward(self, x):
        (x_even_update, x_odd_update, saida) = self.level(x)
        return (x_even_update, x_odd_update, saida)


class LevelSCINet(nn.Module):
    def __init__(self, side_tree, num_levels, c_level, in_planes, kernel_size, dropout, groups, hidden_size, INN, dataset):
        super(LevelSCINet, self).__init__()
        self.interact = InteractorLevel(side_tree=side_tree, num_levels=num_levels, c_level=c_level,
                                        in_planes=in_planes, kernel=kernel_size, dropout=dropout, groups=groups,
                                        hidden_size=hidden_size, INN=INN, dataset=dataset)

    def checkShape(self, list, shape_atual):
        sum = 0
        for x in list:
            sum+= x.shape[2]
        return sum == shape_atual

    def forward(self, x):
        (x_even_update, x_odd_update, saida) = self.interact(x)
        return x_even_update.permute(0, 2, 1), x_odd_update.permute(0, 2, 1), saida  # even: B, T, D odd: B, T, D




class SCINet_Tree(nn.Module):
    def __init__(self, in_planes, side_tree, num_levels, current_level, kernel_size, dropout, groups, hidden_size, INN, dataset):
        super().__init__()
        self.current_level = current_level
        self.num_levels = num_levels
        self.side_tree = side_tree
        self.saida = in_planes

        self.workingblock = LevelSCINet(
            side_tree=side_tree,
            num_levels=num_levels,
            c_level=current_level,
            in_planes=in_planes,
            kernel_size=kernel_size,
            dropout=dropout,
            groups=groups,
            hidden_size=hidden_size,
            INN=INN,
            dataset=dataset)

        if current_level != 0:
            self.SCINet_Tree_odd = SCINet_Tree(in_planes, 'L', num_levels, current_level - 1, kernel_size, dropout,
                                               groups, hidden_size, INN, dataset)
            self.SCINet_Tree_even = SCINet_Tree(in_planes, 'R', num_levels, current_level - 1, kernel_size, dropout,
                                                groups, hidden_size, INN, dataset)

    def zip_up_the_pants(self, even, odd):
        even = even.permute(1, 0, 2)
        odd = odd.permute(1, 0, 2)  # L, B, D
        even_len = even.shape[0]
        odd_len = odd.shape[0]
        mlen = min((odd_len, even_len))
        _ = []
        for i in range(mlen):
            _.append(even[i].unsqueeze(0))
            _.append(odd[i].unsqueeze(0))
        if odd_len < even_len:
            _.append(even[-1].unsqueeze(0))
        return torch.cat(_, 0).permute(1, 0, 2)  # B, L, D

    def forward(self, x):
        x_even_update, x_odd_update, saida = self.workingblock(x)
        nova_data = [x_even_update, x_odd_update, saida]
        # We recursively reordered these sub-series. You can run the ./utils/recursive_demo.py to emulate this procedure.
        if self.current_level == 0:
            # Mudar de acordo com o número de canais
            saida = int(x_even_update.shape[2])
            x_even_update_1 = F.adaptive_avg_pool1d(x_even_update, self.saida)
            x_odd_update_2 = F.adaptive_avg_pool1d(x_odd_update, self.saida)
            aa = self.zip_up_the_pants(x_even_update_1, x_odd_update_2)
            return aa
        else:
            return self.zip_up_the_pants(self.SCINet_Tree_even(nova_data), self.SCINet_Tree_odd(nova_data))
            # return self.zip_up_the_pants(self.SCINet_Tree_even(x_even_update), self.SCINet_Tree_odd(x_odd_update))


class EncoderTree(nn.Module):
    def __init__(self, in_planes, side_tree, num_levels, kernel_size, dropout, groups, hidden_size, INN, dataset):
        super().__init__()
        self.levels = num_levels
        self.SCINet_Tree = SCINet_Tree(
            in_planes=in_planes,
            side_tree=side_tree,
            num_levels=num_levels,
            current_level=num_levels - 1,
            kernel_size=kernel_size,
            dropout=dropout,
            groups=groups,
            hidden_size=hidden_size,
            INN=INN,
            dataset=dataset)

    def forward(self, x):
        x = self.SCINet_Tree(x)

        return x


class DESCINet(nn.Module):
    def __init__(self, output_len, input_len, input_dim=9, hid_size=1, num_stacks=1,
                 num_levels=3, num_decoder_layer=1, concat_len=0, groups=1, kernel=5, dropout=0.5,
                 single_step_output_One=0, input_len_seg=0, positionalE=False, modified=True, RIN=False, dataset=None):
        super(DESCINet, self).__init__()

        self.input_dim = input_dim
        self.input_len = input_len
        self.output_len = output_len
        self.hidden_size = hid_size
        self.num_levels = num_levels
        self.groups = groups
        self.modified = modified
        self.kernel_size = kernel
        self.dropout = dropout
        self.single_step_output_One = single_step_output_One
        self.concat_len = concat_len
        self.pe = positionalE
        self.RIN = RIN
        self.num_decoder_layer = num_decoder_layer

        self.blocks1 = EncoderTree(
            in_planes=self.input_dim,
            side_tree='root',
            num_levels=self.num_levels,
            kernel_size=self.kernel_size,
            dropout=self.dropout,
            groups=self.groups,
            hidden_size=self.hidden_size,
            INN=modified,
            dataset=dataset
        )

        if num_stacks == 2:  # we only implement two stacks at most.
            self.blocks2 = EncoderTree(
                in_planes=self.input_dim,
                side_tree='root',
                num_levels=self.num_levels,
                kernel_size=self.kernel_size,
                dropout=self.dropout,
                groups=self.groups,
                hidden_size=self.hidden_size,
                INN=modified,
                dataset=dataset)

        self.stacks = num_stacks

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.bias.data.zero_()
        self.projection1 = nn.Conv1d(self.input_len, self.output_len, kernel_size=1, stride=1, bias=False)
        self.div_projection = nn.ModuleList()
        self.overlap_len = self.input_len // 4
        self.div_len = self.input_len // 6

        if self.num_decoder_layer > 1:
            self.projection1 = nn.Linear(self.input_len, self.output_len)
            for layer_idx in range(self.num_decoder_layer - 1):
                div_projection = nn.ModuleList()
                for i in range(6):
                    lens = min(i * self.div_len + self.overlap_len, self.input_len) - i * self.div_len
                    div_projection.append(nn.Linear(lens, self.div_len))
                self.div_projection.append(div_projection)

        if self.single_step_output_One:  # only output the N_th timestep.
            if self.stacks == 2:
                if self.concat_len:
                    self.projection2 = nn.Conv1d(self.concat_len + self.output_len, 1,
                                                 kernel_size=1, bias=False)
                else:
                    self.projection2 = nn.Conv1d(self.input_len + self.output_len, 1,
                                                 kernel_size=1, bias=False)
        else:  # output the N timesteps.
            if self.stacks == 2:
                if self.concat_len:
                    self.projection2 = nn.Conv1d(self.concat_len + self.output_len, self.output_len,
                                                 kernel_size=1, bias=False)
                else:
                    self.projection2 = nn.Conv1d(self.input_len + self.output_len, self.output_len,
                                                 kernel_size=1, bias=False)

        # For positional encoding
        self.pe_hidden_size = input_dim
        if self.pe_hidden_size % 2 == 1:
            self.pe_hidden_size += 1

        num_timescales = self.pe_hidden_size // 2
        max_timescale = 10000.0
        min_timescale = 1.0

        log_timescale_increment = (
                math.log(float(max_timescale) / float(min_timescale)) /
                max(num_timescales - 1, 1))
        temp = torch.arange(num_timescales, dtype=torch.float32)
        inv_timescales = min_timescale * torch.exp(
            torch.arange(num_timescales, dtype=torch.float32) *
            -log_timescale_increment)
        self.register_buffer('inv_timescales', inv_timescales)

        ### RIN Parameters ###
        if self.RIN:
            self.affine_weight = nn.Parameter(torch.ones(1, 1, input_dim))
            self.affine_bias = nn.Parameter(torch.zeros(1, 1, input_dim))

    def get_position_encoding(self, x):
        max_length = x.size()[1]
        position = torch.arange(max_length, dtype=torch.float32,
                                device=x.device)  # tensor([0., 1., 2., 3., 4.], device='cuda:0')
        temp1 = position.unsqueeze(1)  # 5 1
        temp2 = self.inv_timescales.unsqueeze(0)  # 1 256
        scaled_time = position.unsqueeze(1) * self.inv_timescales.unsqueeze(0)  # 5 256
        signal = torch.cat([torch.sin(scaled_time), torch.cos(scaled_time)], dim=1)  # [T, C]
        signal = F.pad(signal, (0, 0, 0, self.pe_hidden_size % 2))
        signal = signal.view(1, max_length, self.pe_hidden_size)

        return signal

    def forward(self, x):
        assert self.input_len % (np.power(2,
                                          self.num_levels)) == 0  # evenly divided the input length into two parts. (e.g., 32 -> 16 -> 8 -> 4 for 3 levels)
        if self.pe:
            pe = self.get_position_encoding(x)
            if pe.shape[2] > x.shape[2]:
                x += pe[:, :, :-1]
            else:
                x += self.get_position_encoding(x)

        ### activated when RIN flag is set ###
        if self.RIN:
            print('/// RIN ACTIVATED ///\r', end='')
            means = x.mean(1, keepdim=True).detach()
            # mean
            x = x - means
            # var
            stdev = torch.sqrt(torch.var(x, dim=1, keepdim=True, unbiased=False) + 1e-5)
            x /= stdev
            # affine
            # print(x.shape,self.affine_weight.shape,self.affine_bias.shape)
            x = x * self.affine_weight + self.affine_bias

        # the first stack
        res1 = x
        nova_entrada = [res1, x]

        x = self.blocks1(nova_entrada[0])
        x += res1
        if self.num_decoder_layer == 1:
            x = self.projection1(x)
        else:
            x = x.permute(0, 2, 1)
            for div_projection in self.div_projection:
                # se tiver cuda
                if torch.cuda.is_available():
                    output = torch.zeros(x.shape, dtype=x.dtype).cuda()
                else:
                    output = torch.zeros(x.shape, dtype=x.dtype)
                for i, div_layer in enumerate(div_projection):
                    div_x = x[:, :, i * self.div_len:min(i * self.div_len + self.overlap_len, self.input_len)]
                    output[:, :, i * self.div_len:(i + 1) * self.div_len] = div_layer(div_x)
                x = output
            x = self.projection1(x)
            x = x.permute(0, 2, 1)

        if self.stacks == 1:
            ### reverse RIN ###
            if self.RIN:
                x = x - self.affine_bias
                x = x / (self.affine_weight + 1e-10)
                x = x * stdev
                x = x + means

            return x

        elif self.stacks == 2:
            MidOutPut = x
            if self.concat_len:
                x = torch.cat((res1[:, -self.concat_len:, :], x), dim=1)
            else:
                x = torch.cat((res1, x), dim=1)

            # the second stack
            res2 = x
            x = self.blocks2(x)
            x += res2
            x = self.projection2(x)

            ### Reverse RIN ###
            if self.RIN:
                MidOutPut = MidOutPut - self.affine_bias
                MidOutPut = MidOutPut / (self.affine_weight + 1e-10)
                MidOutPut = MidOutPut * stdev
                MidOutPut = MidOutPut + means

            if self.RIN:
                x = x - self.affine_bias
                x = x / (self.affine_weight + 1e-10)
                x = x * stdev
                x = x + means

            return x, MidOutPut


def get_variable(x):
    x = Variable(x)
    return x.cuda() if torch.cuda.is_available() else x


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--window_size', type=int, default=96)
    parser.add_argument('--horizon', type=int, default=12)

    parser.add_argument('--dropout', type=float, default=0.5)
    parser.add_argument('--groups', type=int, default=1)

    parser.add_argument('--hidden-size', default=1, type=int, help='hidden channel of module')
    parser.add_argument('--INN', default=1, type=int, help='use INN or basic strategy')
    parser.add_argument('--kernel', default=3, type=int, help='kernel size')
    parser.add_argument('--dilation', default=1, type=int, help='dilation')
    parser.add_argument('--positionalEcoding', type=bool, default=True)

    parser.add_argument('--single_step_output_One', type=int, default=0)

    args = parser.parse_args()

    model = DESCINet(output_len=args.horizon, input_len=args.window_size, input_dim=9, hid_size=args.hidden_size,
                     num_stacks=1,
                     num_levels=3, concat_len=0, groups=args.groups, kernel=args.kernel, dropout=args.dropout,
                     single_step_output_One=args.single_step_output_One, positionalE=args.positionalEcoding,
                     modified=True).cuda()
    x = torch.randn(32, 96, 9).cuda()
    y = model(x)
    print(y.shape)